﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inventoryTrigger : MonoBehaviour
{

    FixedJoint fj;
    private void Start()
    {
        fj = gameObject.GetComponent<FixedJoint>();
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("ui triggerd");
       
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "PickUp" && other.GetComponent<Rigidbody>().isKinematic == false)
        {
            other.transform.position = fj.transform.position;
            fj.connectedBody = other.GetComponent<Rigidbody>();
        }
    }

    void OnTriggerExit(Collider other)
    {
        Debug.Log("trigger exit");
        if (other.tag == "PickUp")
        {
            fj.connectedBody = null;
        }
        else
        {
            other.transform.position = fj.transform.position;
            fj.connectedBody = other.GetComponent<Rigidbody>();
        }
    }
}
