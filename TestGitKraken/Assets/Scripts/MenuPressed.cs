﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuPressed : MonoBehaviour {

    private SteamVR_TrackedObject trackedObj;
    public SteamVR_Controller.Device device;

    public Canvas canvas;

    [SerializeField] GameObject leftController, rightController;
    [SerializeField] RigControl cameraRig;

	// Use this for initialization
	void Start () {
        trackedObj = GetComponent<SteamVR_TrackedObject>();

        canvas.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {

        device = SteamVR_Controller.Input((int)trackedObj.index);

        if(gameObject == leftController)
        {
            if (device.GetPress(SteamVR_Controller.ButtonMask.Grip))
            {
                canvas.gameObject.SetActive(true);
            }
            else
            {
                canvas.gameObject.SetActive(false);
            }
        }

        /*if (Input.GetKeyDown(KeyCode.I))
        {
            canvas.gameObject.SetActive(true);
        }
        if (Input.GetKeyUp(KeyCode.I))
        {
            canvas.gameObject.SetActive(false);
        }*/
    }
}
