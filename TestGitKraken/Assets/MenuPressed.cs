﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuPressed : MonoBehaviour
{

    private SteamVR_TrackedController device;

    public Canvas canvas;

    // Use this for initialization
    void Start()
    {
        device.GetComponent<SteamVR_TrackedController>();
        device.Gripped += Menu;
        device.Ungripped += NoMenu;
        canvas.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            canvas.gameObject.SetActive(true);
        }
        if (Input.GetKeyUp(KeyCode.I))
        {
            canvas.gameObject.SetActive(false);
        }
    }

    void Menu(object sender, ClickedEventArgs e)
    {
        canvas.gameObject.SetActive(true);
    }

    void NoMenu(object sender, ClickedEventArgs e)
    {
        canvas.gameObject.SetActive(false);
    }

}
