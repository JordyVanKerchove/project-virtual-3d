﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class testscript : MonoBehaviour {

    public float XRotation;
    public float YRotation;
    public float ZRotation;

    // Update is called once per frame
    void Update () {
        XRotation = gameObject.transform.eulerAngles.x;
        YRotation = gameObject.transform.eulerAngles.y;
        ZRotation = gameObject.transform.eulerAngles.z;
	}
}
