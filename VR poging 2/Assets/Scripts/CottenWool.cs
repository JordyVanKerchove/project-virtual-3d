﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CottenWool : MonoBehaviour {

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.name == "Disinfectant_Opening")
        {
            Debug.Log("cotton-wool & disinfectant");
            this.gameObject.name = "Cotton-wool-disinfected";
        }
    }
}
