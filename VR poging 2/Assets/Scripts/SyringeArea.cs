﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyringeArea : MonoBehaviour {

    FillSyringe Syringe;
    GameObject Bandage;

    public float neededVolume;

    bool hasBeenInjected;
    bool bandageIsApplied;

	void Start ()
    {
        hasBeenInjected = false;
        bandageIsApplied = false;
	}
	
	void Update ()
    {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (!hasBeenInjected && collision.gameObject.name == "syringe_Filled")
        {
            Syringe = collision.gameObject.GetComponent<FillSyringe>();
            if(Syringe.volume == neededVolume)
            {
                Debug.Log("Right amount of medicine");
            }
            else
            {
                if(Syringe.volume < neededVolume)
                {
                    Debug.Log("To little medicine");
                }
                else
                {
                    Debug.Log("To mush medicine");
                }
            }
            hasBeenInjected = true;
        }

        if (hasBeenInjected && !bandageIsApplied && collision.gameObject.name == "Bandaid")
        {
            Debug.Log("bandaid applied");
            Bandage = collision.gameObject;
            Bandage.transform.SetParent(this.gameObject.transform, false);
        }
    }
}
