﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System;
public class inventoryTrigger : MonoBehaviour
{
    List<FixedJoint> fixedJointList = new List<FixedJoint>();

    private void OnTriggerEnter(Collider other)
    {
        /* for (int i = 0; i < FixedJointArray.Length; i++)
         {
             if (FixedJointArray[i].connectedBody == null)
             {
                 Destroy(FixedJointArray[i]);
             }
         }
        if (other.tag == "PickUp")
        {
            if (IsAttached(other.gameObject.GetInstanceID()))
            {
                Debug.Log("already in inventory");
            }
            else
            {

                if (!other.GetComponent<Rigidbody>().isKinematic)
                {
                    FixedJoint f = gameObject.AddComponent<FixedJoint>();
                    f.connectedBody = other.GetComponent<Rigidbody>();
                    //Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
                    //rb.useGravity = false;
                    fixedJointList.Add(f);

                }
            }
        }*/
    }

      private void OnTriggerStay(Collider other)
      {
        /*  if (other.tag == "PickUp")
          {

                  if (other.GetComponent<Rigidbody>().isKinematic == false && objectCollection.Contains(other.gameObject) == false)
                  {
                      //Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
                      //rb.useGravity = false;
                      FixedJointArray = GetComponents<FixedJoint>();
                      FixedJointArray[FixedJointArray.Length - 1].connectedBody = other.GetComponent<Rigidbody>();
                      objectCollection.Add(other.gameObject);
                  }

          }*/
        if (other.tag == "PickUp")
        {
            if (IsAttached(other.gameObject.GetInstanceID()))
            {
                Debug.Log("already in inventory");
                
            }
            else
            {

                if (!other.GetComponent<Rigidbody>().isKinematic)
                {
                    FixedJoint f = gameObject.AddComponent<FixedJoint>();
                    f.connectedBody = other.GetComponent<Rigidbody>();
               
                   
                    //Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
                    //rb.useGravity = false;
                    fixedJointList.Add(f);
                    Debug.Log("Adding fixedjoint " + other.name);       
                }
            }
        }
    }

      void OnTriggerExit(Collider other)
      {

        /* if (other.tag == "PickUp")
         {
             if (objectCollection.Contains(other.gameObject) == true)
             {
                 FixedJointArray = GetComponents<FixedJoint>();
                 for (int i = 0; i < FixedJointArray.Length; i++)
                 {
                     FixedJointArray = GetComponents<FixedJoint>();
                     if (other.gameObject.name == FixedJointArray[i].connectedBody.name)
                     {
                         //Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
                         //rb.useGravity = true;
                         Destroy(FixedJointArray[i]);
                         objectCollection.Remove(other.gameObject);                    
                     }
                 }
             }
         }*/

        if (other.tag == "PickUp" )
        {
            FixedJoint f = GetJointForObject(other.gameObject.GetInstanceID());
            if(f != null)
            {
                f.connectedBody = null;
                fixedJointList.Remove(f);
                Destroy(f);
            }
        }
      }

    bool IsAttached(int id)
    {
        foreach (FixedJoint fj in fixedJointList)
        {
            if (fj.connectedBody.gameObject.GetInstanceID() == id)
            {
                return true;
            }
        }
        return false;
    }

    FixedJoint GetJointForObject(int id)
    {
        foreach (FixedJoint fj in fixedJointList)
        {
            if (fj.connectedBody.gameObject.GetInstanceID() == id)
            {
                return fj;
            }
        }
        return null;
    }
}
