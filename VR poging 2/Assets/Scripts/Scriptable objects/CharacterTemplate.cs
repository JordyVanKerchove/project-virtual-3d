﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Character Data")]
public class CharacterTemplate : ScriptableObject {
    public string voornaam;
    public string achternaam;
    public string geboortedatum;
    public float gewicht;
    public string geslacht;
    public string specialeOpmerking;
}
