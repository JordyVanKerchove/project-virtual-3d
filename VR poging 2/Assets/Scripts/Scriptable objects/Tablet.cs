﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Tablet : MonoBehaviour {
    public CharacterTemplate chr;
    public CharacterTemplate chrTwee;
    public CharacterTemplate chrDrie;
    public TextMeshProUGUI totaleText;

    // Use this for initialization
    private void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == chr.name)
        {
            totaleText.text = "Voornaam: " + chr.voornaam + "\n Naam: " + chr.achternaam
                         + "\n Geboortedatum: " + chr.geboortedatum + "\n Gewicht: " + chr.gewicht
                         + "\n Geslacht: " + chr.geslacht + "\n Speciale opmerking: "
                         + chr.specialeOpmerking;
        }
        if (other.tag == chrTwee.name)
        {
            totaleText.text = "Voornaam: " + chrTwee.voornaam + "\n Naam: " + chrTwee.achternaam
                         + "\n Geboortedatum: " + chrTwee.geboortedatum + "\n Gewicht: " + chrTwee.gewicht
                         + "\n Geslacht: " + chrTwee.geslacht + "\n Speciale opmerking: "
                         + chrTwee.specialeOpmerking;
        }
        if (other.tag == chrDrie.name)
        {
            totaleText.text = "Voornaam: " + chrDrie.voornaam + "\n Naam: " + chrDrie.achternaam
                         + "\n Geboortedatum: " + chrDrie.geboortedatum + "\n Gewicht: " + chrDrie.gewicht
                         + "\n Geslacht: " + chrDrie.geslacht + "\n Speciale opmerking: "
                         + chrDrie.specialeOpmerking;
        }
       
    }
    
}
