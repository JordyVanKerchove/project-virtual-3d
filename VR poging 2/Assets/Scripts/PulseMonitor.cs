﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PulseMonitor : MonoBehaviour {

    Animator anim;

    private GameObject arm;
    public GameObject anchor;

    public TextMeshProUGUI SysText;
    public TextMeshProUGUI DiaText;
    public TextMeshProUGUI PulseText;

    private bool isAttached;

    public bool isClosing;

	void Start ()
    {
        isAttached = false;

        anim = this.gameObject.GetComponent<Animator>();
        
        isClosing = false;
	}
	
	void Update ()
    {
        anim.SetBool("IsClosing", isClosing);

        if (isAttached)
        {
            SysText.text = "124";
            DiaText.text = "76";
            PulseText.text = "82";

            this.transform.position = new Vector3(anchor.transform.position.x, anchor.transform.position.y , anchor.transform.position.z);
            this.transform.eulerAngles = new Vector3(anchor.transform.rotation.x+90, anchor.transform.rotation.y, anchor.transform.rotation.z);
        }
        else
        {
            SysText.text = "";
            DiaText.text = "";
            PulseText.text = "";
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Arm")
        {
            Debug.Log("Band attached");
            isAttached = true;

            arm = other.gameObject;

            anchor = other.gameObject.transform.GetChild(0).gameObject;

            //Instantiate(anchor, new Vector3(0, 0, 0), Quaternion.identity);
            //anchor.transform.position = arm.transform.position;
            //anchor.transform.rotation = arm.transform.rotation;
            //anchor.transform.parent = arm.transform;

            Rigidbody rb = GetComponent<Rigidbody>();
            rb.useGravity = false;
            
            isClosing = true;
            Debug.Log("true");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Arm")
        {
            //Destroy(anchor);

            Debug.Log("Band released");
            Rigidbody rb = GetComponent<Rigidbody>();
            rb.useGravity = true;
            isAttached = false;

            isClosing = false;
            Debug.Log("false");
        }
    }
}
