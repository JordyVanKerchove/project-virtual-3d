﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class FillSyringe : MonoBehaviour {

    bool isCollided;

    int nbrOfSeconds;
    public double volume;

    public TextMeshProUGUI volumeText;
    public Canvas canvas;

    public double timer;

    private void Start()
    {
        isCollided = false;
        nbrOfSeconds = 0;
        timer = 0d;
        canvas.enabled = false;
    }

    private void Update()
    {
        if (isCollided)
        {
            Debug.Log("is filling");
            timer += Time.deltaTime;
            volume = (timer) * 0.25d;
            volume = Math.Round(volume, 1);

            volumeText.text = volume.ToString() + " ml";
        }

        this.name = "syringe_Filled";
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.name == "MedicineBottleTop")
        {
            Debug.Log("on trigger enter");
            canvas.enabled = true;
            isCollided = true;
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.name == "MedicineBottleTop")
        {
            Debug.Log("on trigger exit");
            isCollided = false;
        }
    }

    public void EmptySyringe()
    {
        if (volume > 0)
        {
            timer -= Time.deltaTime;
            volume = (timer) * 0.25d;
            volume = Math.Round(volume, 1);
        }
        if (volume < 0)
        {
            volume = 0;
        }

        //Debug.Log(volume); 

        volumeText.text = volume.ToString() + " ml";
    }
}
