﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectSyringeArea : MonoBehaviour {

    FillSyringe Syringe;
    GameObject Bandaid;
    public GameObject anchor;
    public GameObject bloodParticleEmiter;
    public GameObject bloodPool;

    public float neededVolume;

    private Material material;
    private Color BeginColor;

    bool isDisinfected;
    bool hasBeenInjected;
    bool bandageIsApplied;

    bool isEmptyingSyringe;

    private void Start()
    {
        isDisinfected = false;
        hasBeenInjected = false;
        bandageIsApplied = false;

        material = this.GetComponent<MeshRenderer>().material;
        BeginColor = material.color;
    }

    private void Update()
    {
        if(isEmptyingSyringe)
        {
            Debug.Log("Emptying");
            Syringe.EmptySyringe();
        }

        if(bandageIsApplied)
        {
            Bandaid.transform.position = anchor.transform.position;
            Bandaid.transform.rotation = anchor.transform.rotation; 
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.name == "Cotton-wool-disinfected")
        {
            Debug.Log("skin & cotton-wool");

            isDisinfected = true;

            material.color = new Color(66,244,66);
        }

        if (collision.gameObject.name == "syringe_Filled")
        {
            Debug.Log("skin & syringe");
            Syringe = collision.gameObject.GetComponent<FillSyringe>();

            Syringe.timer = Syringe.volume * 4;
            isEmptyingSyringe = true;

            if(Syringe.volume > 0)
            {
                Syringe.EmptySyringe();
            }
            if(Syringe.volume < 0)
            {
                Syringe.volume = 0;
            }

            if (Syringe.volume == neededVolume)
            {
                Debug.Log("Right amount of medicine");
            }
            else
            {
                if (Syringe.volume < neededVolume)
                {
                    Debug.Log("To little medicine");
                }
                else
                {
                    Debug.Log("To much medicine");
                }
            }
            hasBeenInjected = true;

            material.color = new Color(255, 55, 0);
        }

        if (collision.gameObject.name == "Bandaid")
        {
            Debug.Log("bandaid applied");
            Bandaid = collision.gameObject;
            //Bandaid.transform.SetParent(this.gameObject.transform, false);

            Instantiate(anchor, new Vector3(0, 0, 0), Quaternion.identity);
            anchor.transform.position = Bandaid.transform.position;
            anchor.transform.rotation = Bandaid.transform.rotation;
            anchor.transform.parent = this.transform;

            Rigidbody rb = Bandaid.gameObject.GetComponent<Rigidbody>();
            rb.useGravity = false;
            //rb.constraints = RigidbodyConstraints.FreezeAll;

            material.color = new Color(255, 55, 0);

            bandageIsApplied = true;
            bloodParticleEmiter.GetComponent<ParticleSystem>().Pause();
            bloodParticleEmiter.SetActive(false);
            bloodPool.GetComponent<ParticleSystem>().Pause();
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        if (isDisinfected && collision.gameObject.name == "syringe_Filled")
        {
            Debug.Log("Exit skin");
            isEmptyingSyringe = false;
        }
        if ((collision.gameObject.name == "syringe_Filled") || (collision.gameObject.name == "Syringe"))
        {
            bloodParticleEmiter.GetComponent<ParticleSystem>().Play();
            bloodParticleEmiter.SetActive(true);
            bloodPool.GetComponent<ParticleSystem>().Play();
        }

        if (hasBeenInjected && collision.gameObject.name == "Bandaid")
        {
            
        }
    }
}
