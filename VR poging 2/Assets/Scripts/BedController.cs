﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BedController : MonoBehaviour {

    public bool isBedUpPressed = false;
    public bool isBedDownPressed = false;

    public bool isBedHeadUpPressed = false;
    public bool isBedHeadDownPressed = false;

    public bool isBedFeetUpPressed = false;
    public bool isBedFeetDownPressed = false;

    public int bedUpDownSpeed = 5;
    public int bedHeadUpDownSpeed = 5;
    public int bedFeetUpDownspeed = 5;

    public GameObject Cube_010;
    public GameObject Cube_011;
    public GameObject Cube_003;
    public GameObject Cube_004;
    public GameObject Armature;
    public GameObject Cube_012;
    public GameObject Cube_013;

    public GameObject Bone_001;
    public GameObject Bone_002;
    public GameObject Cube_020;
    public GameObject Cube_001;

    public GameObject Bone;
    public GameObject Bone_003;
    public GameObject Cube_019;
    public GameObject Cube_018;


// Use this for initialization
    void Start () {
        //Bone_001.transform.eulerAngles.z = 360f;
        Bone_001.transform.Rotate(0, 0, 359.99f);

        if (bedUpDownSpeed <= 0)
        {
            bedUpDownSpeed = 1;
        }
        if (bedHeadUpDownSpeed <= 0)
        {
            bedHeadUpDownSpeed = 1;
        }
        if (bedFeetUpDownspeed <= 0)
        {
            bedFeetUpDownspeed = 1;
        }
	}
	
	// Update is called once per frame
	void Update () {
        BedUpDown();
        BedHeadUpDown();
	}

    void BedUpDown()
    {
        float Cube_010_YMaxPos = 0.6104463f;
        float Cube_010_YMinPos = 0.3595378f;

        float Cube_011_YMaxPos = 0.4569158f;
        float Cube_011_YMinPos = 0.3202636f;

        float Cube_003_YMaxPos;
        float Cube_003_YMinPos;

        float Cube_004_YMaxPos;
        float Cube_004_YMinPos;

        float Armature_YMaxPos = 0.7392311f;
        float Armature_YMinPos = 0.4892333f;

        //float Cube_012_YMaxPos = 0.6544324f;
        //float Cube_012_YMinPos = 0.4037654f;

        //float Cube_013_YMaxPos = 0.6544324f;
        //float Cube_013_YMinPos = 0.4037654f;

        Cube_003_YMaxPos = Cube_011_YMaxPos;
        Cube_003_YMinPos = Cube_011_YMinPos;

        Cube_004_YMaxPos = Cube_010_YMaxPos;
        Cube_004_YMinPos = Cube_010_YMinPos;

        if (isBedUpPressed)
        {
            if(Armature.transform.position.y <= Armature_YMaxPos)
            {
                Armature.transform.Translate(Vector3.up * Time.deltaTime / bedUpDownSpeed, Space.World);
                Cube_012.transform.Translate(Vector3.up * Time.deltaTime / bedUpDownSpeed, Space.World);
                Cube_013.transform.Translate(Vector3.up * Time.deltaTime / bedUpDownSpeed, Space.World);
            }

            if (Cube_011.transform.position.y <= Cube_011_YMaxPos)
            {
                Cube_011.transform.Translate(Vector3.up * Time.deltaTime / bedUpDownSpeed, Space.World);
                Cube_003.transform.Translate(Vector3.up * Time.deltaTime / bedUpDownSpeed, Space.World);
            }

            if (Cube_010.transform.position.y <= Cube_010_YMaxPos)
            {
                Cube_010.transform.Translate(Vector3.up * Time.deltaTime / bedUpDownSpeed, Space.World);
                Cube_004.transform.Translate(Vector3.up * Time.deltaTime / bedUpDownSpeed, Space.World);
            }
        }

        if (isBedDownPressed)
        {
            if (Armature.transform.position.y >= Armature_YMinPos)
            {
                Armature.transform.Translate(Vector3.up * -Time.deltaTime / bedUpDownSpeed, Space.World);
                Cube_012.transform.Translate(Vector3.up * -Time.deltaTime / bedUpDownSpeed, Space.World);
                Cube_013.transform.Translate(Vector3.up * -Time.deltaTime / bedUpDownSpeed, Space.World);
            }
            if (Cube_011.transform.position.y >= Cube_011_YMinPos)
            {
                Cube_011.transform.Translate(Vector3.up * -Time.deltaTime / bedUpDownSpeed, Space.World);
                Cube_003.transform.Translate(Vector3.up * -Time.deltaTime / bedUpDownSpeed, Space.World);
            }
            if (Cube_010.transform.position.y >= Cube_010_YMinPos)
            {
                Cube_010.transform.Translate(Vector3.up * -Time.deltaTime / bedUpDownSpeed, Space.World);
                Cube_004.transform.Translate(Vector3.up * -Time.deltaTime / bedUpDownSpeed, Space.World);
            }
        }
    }

    void BedHeadUpDown()
    {
        float Bone_001_ZMaxRot = 311.4155f;
        //float Bone_001_ZMinRot = 6.830189e-06f;

        float Bone_002_ZMaxRot = 354.717f;
        //float Bone_002_ZMinRot = 1.024528e-05f;

        //--
        float Cube_012_XMaxRot = 300.43598f;
        float Cube_012_YMaxRot = 270f;
        float Cube_012_ZMaxRot = 90f;

        float Cube_012_XMinRot = 270f;
        float Cube_012_YMinRot = 0f;
        float Cube_012_ZMinRot = 0f;
        //--

        float Cube_001_YMaxRot = 270f;
        float Cube_001_YMinRot = 0f;

        if (isBedHeadUpPressed)
        {
            Debug.Log("z" + Cube_001.transform.rotation.eulerAngles.x);
            //Debug.Log("y" + Cube_012.transform.rotation.eulerAngles.y);
            //Debug.Log("z" + Cube_012.transform.rotation.eulerAngles.z);
            //Cube_012.transform.Rotate(Time.deltaTime * bedHeadUpDownSpeed, 0, 0, Space.World);
            //Cube_012.transform.Rotate(0, Time.deltaTime * bedHeadUpDownSpeed, 0, Space.World);
            //Cube_012.transform.Rotate(0, 0, Time.deltaTime * bedHeadUpDownSpeed, Space.World);

            if ((Bone_001.transform.eulerAngles.z > Bone_001_ZMaxRot))
            {
                Bone_001.transform.Rotate(0, 0, Time.deltaTime * bedHeadUpDownSpeed, Space.World);
            }
            if (Bone_002.transform.eulerAngles.z > Bone_002_ZMaxRot)
            {
                Bone_002.transform.Rotate(0, 0, Time.deltaTime * (bedHeadUpDownSpeed - 2.5f), Space.World);
            }
            //if ((Bone_002.transform.rotation.z > 1f) && (Bone_002.transform.rotation.z < 20f))
            //{
            //    Bone_002.transform.eulerAngles = new Vector3(360, 0, 0);
            //}
            if (Cube_012.transform.eulerAngles.x < Cube_012_XMaxRot)
            {
                //Cube_012.transform.Rotate(Time.deltaTime * bedHeadUpDownSpeed, 0, 0, Space.World);
                //Cube_012.transform.Rotate(0, Time.deltaTime * bedHeadUpDownSpeed, 0, Space.World);
                Cube_012.transform.Rotate(0, 0, Time.deltaTime * (bedHeadUpDownSpeed - 1f), Space.World);
            }
            if ((Bone_001.transform.eulerAngles.z < 320f) && (Cube_001.transform.eulerAngles.x < 321f))
            {
                Cube_001.transform.Rotate(0, 0, Time.deltaTime * bedHeadUpDownSpeed, Space.World);
            }
        }

        if (isBedHeadDownPressed)
        {
            Debug.Log(Bone_001.transform.localRotation.eulerAngles.y);
            if ((Bone_001.transform.eulerAngles.y <= 354.5f))
            {
                Bone_001.transform.Rotate(Vector3.forward, -Time.deltaTime * bedHeadUpDownSpeed, Space.World);
            }
            if (Bone_001.transform.eulerAngles.z < 20f)
            {
                Bone_001.transform.eulerAngles = new Vector3(180, 0, 180);
            }
            if ((Bone_002.transform.eulerAngles.z < 358f) && (Bone_002.transform.eulerAngles.z > 20f))
            {
                Bone_002.transform.Rotate(Vector3.forward, -Time.deltaTime * (bedHeadUpDownSpeed - 2.5f), Space.World);
            }
            if ((Bone_002.transform.eulerAngles.z < 20))
            {
                Bone_002.transform.eulerAngles = new Vector3(180, 0, 180);
            }
            if (Cube_012.transform.eulerAngles.x > 270.5f)
            {
                //Cube_012.transform.Rotate(Time.deltaTime * bedHeadUpDownSpeed, 0, 0, Space.World);
                //Cube_012.transform.Rotate(0, Time.deltaTime * bedHeadUpDownSpeed, 0, Space.World);
                Cube_012.transform.Rotate(0, 0, -Time.deltaTime * (bedHeadUpDownSpeed + 0.5f), Space.World);
            }
            //if ((Cube_001.transform.eulerAngles.x > 275f))
            //{
            //    Cube_001.transform.Rotate(0, 0, -Time.deltaTime * bedHeadUpDownSpeed, Space.World);
            //}
        }

    }

    void BedFeetUpDown()
    {
        float Bone_ZMaxRot = 340.4493f;
        //float Bone_ZMidRot = -3.415094e-06f;
        float Bone_ZMinRot = 2.841969f;

        float Bone_003_ZMaxRot = 9.51402f;
        //float Bone_003_ZMidRot = 1.024528e-05f;
        float Bone_003_ZMinRot = 9.557346f;

        //--
        float Cube_013_XMaxRot = 276.0627f;
        float Cube_013_YMaxRot = 90f;
        float Cube_013_ZMaxRot = 270f;

        //float Cube_013_XMidRot = 270f;
        //float Cube_013_YMidRot = 0f;
        //float Cube_013_ZMidRot = 0f;

        float Cube_013_XMinRot = 277.0742f;
        float Cube_013_YMinRot = 270f;
        float Cube_013_ZMinRot = 90f;
        //--

        float Cube_019_YMaxRot = 90f;
        //float Cube_019_YMidRot = 0f;
        float Cube_019_YMinRot = 270f;

        float Cube_018_YMaxRot = 90f;
        //float Cube_018_YMidRot = 0f;
        float Cube_018_YMinRot = 270f;



    }
}
