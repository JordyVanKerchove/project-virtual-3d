﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleLauncher : MonoBehaviour {

    public ParticleSystem particleLauncher;
    public ParticleDecalPool particleDecalPool;

    List<ParticleCollisionEvent> collisionEvents;

	// Use this for initialization
	void Start () {

        collisionEvents = new List<ParticleCollisionEvent>();

	}

    // Update is called once per frame
    void Update()
    {

    }

    private void OnParticleCollision(GameObject other)
    {
        ParticlePhysicsExtensions.GetCollisionEvents(particleLauncher, other, collisionEvents);
        for (int i = 0; i < collisionEvents.Count; i++)
        {
            particleDecalPool.ParticleHit(collisionEvents[i]);
        }
    }
}
