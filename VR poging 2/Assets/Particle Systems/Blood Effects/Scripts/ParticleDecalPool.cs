﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDecalPool : MonoBehaviour {

    private int particleDecalDataIndex;
    private BloodParticleData[] bloodParticleData;
    private ParticleSystem.Particle[] particles;
    private ParticleSystem bloodDecalParticleSystem;

    public int maxDecals = 100;
    public float decalSizeMin = 0.5f;
    public float decalSizeMax = 1.5f;

	// Use this for initialization
	void Start () {
        bloodDecalParticleSystem = GetComponent<ParticleSystem>();
        particles = new ParticleSystem.Particle[maxDecals];
        bloodParticleData = new BloodParticleData[maxDecals];
        for (int i = 0; i < maxDecals; i++)
        {
            bloodParticleData[i] = new BloodParticleData();
        }
	}
	
    public void ParticleHit(ParticleCollisionEvent particleCollisionEvent)
    {
        SetParticleData(particleCollisionEvent);
        DisplayParticles();
    }

	void SetParticleData(ParticleCollisionEvent particleCollisionEvent)
    {
        if (particleDecalDataIndex >= maxDecals)
        {
            particleDecalDataIndex = 0;
        }

        //record collision position, rotation, size and color

        bloodParticleData[particleDecalDataIndex].position = particleCollisionEvent.intersection;
        Vector3 particleRotationEuler = Quaternion.LookRotation(particleCollisionEvent.normal).eulerAngles;
        particleRotationEuler.z = Random.Range(0, 360);
        bloodParticleData[particleDecalDataIndex].rotation = particleRotationEuler;
        bloodParticleData[particleDecalDataIndex].size = Random.Range(decalSizeMin, decalSizeMax);


        particleDecalDataIndex++;
    }

    void DisplayParticles()
    {
        for (int i = 0; i < bloodParticleData.Length; i++)
        {
            particles[i].position = bloodParticleData[i].position;
            particles[i].rotation3D = bloodParticleData[i].rotation;
            particles[i].startSize = bloodParticleData[i].size;
        }
        bloodDecalParticleSystem.SetParticles(particles, particles.Length);
    }
}
