﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;

public class ViveInput : MonoBehaviour {

    [SteamVR_DefaultAction("Squeeze")]

	public SteamVR_Input_Sources handtype;
	public SteamVR_Action_Boolean grabAction;

    public GameObject canvas;
    private bool canvasIsActive;
    private bool readyForClick;

    void Start()
    {
        canvas.SetActive(false);
        canvasIsActive = false;
        readyForClick = true;
    }

    void Update ()
    {
		if (grabAction.GetState(handtype))
        {
            if(readyForClick)
            {
                if (canvasIsActive)
                {
                    canvas.SetActive(false);
                    canvasIsActive = false;
                }
                else
                {
                    canvas.SetActive(true);
                    canvasIsActive = true;
                }
            }

            readyForClick = false;
        }
        else
        {
            readyForClick = true;
        }

        if (Input.GetKeyDown(KeyCode.I))
        {
            if (canvasIsActive)
            {
                canvas.SetActive(false);
                canvasIsActive = false;
            }
            else
            {
                canvas.SetActive(true);
                canvasIsActive = true;
            }
        }
    }
}
